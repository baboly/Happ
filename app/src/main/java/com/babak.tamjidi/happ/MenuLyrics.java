package com.babaktamjidi.happ;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class MenuLyrics extends Fragment {

    View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.lyrics_layout, container, false);

        ((MainActivity) getActivity()).setActionBarTitle(getString(R.string.title_lattexter));

        return rootView;
    }
}
