package com.babaktamjidi.happ;

import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.babaktamjidi.happ.repository.Landmark;
import com.babaktamjidi.happ.repository.LandmarkMockup;

import java.util.ArrayList;

public class MenuFootspar extends Fragment
        implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener,
        android.location.LocationListener {

    private final String TAG = MenuFootspar.class.getSimpleName();
    private LandmarkMockup landmarkMockup = new LandmarkMockup();
    private ArrayList<Landmark> landmarks = landmarkMockup.getLandmarks();

    private static final int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private GoogleApiClient googleApiClient;
    private LocationManager locationManager;
    private LocationRequest locationRequest;
    private MapView mapView;
    private GoogleMap map;
    private String provider;
    private LinearLayout popup;
    private TextView circleInfo;
    private TextView circleTitle;
    private View view;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        ((MainActivity) getActivity()).setActionBarTitle(getString(R.string.title_fotspar));

        view = inflater.inflate(R.layout.footspar_layout, container, false);

        Criteria criteria = new Criteria();
        this.locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        provider = locationManager.getBestProvider(criteria, false);

        popup = (LinearLayout) view.findViewById(R.id.popup);
        popup.setVisibility(View.INVISIBLE);

        mapView = (MapView) view.findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.onResume();
        map = mapView.getMap();
        map.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
            @Override
            public boolean onMyLocationButtonClick() {
                return false;
            }
        });

        setMapView();
        buildGoogleApiClient();

        locationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10 * 1000).setFastestInterval(10 * 1000);
        return view;
    }

    private void setMapView() {
        try {
            MapsInitializer.initialize(getActivity());
            switch (GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity())) {
                case ConnectionResult.SUCCESS:
                    if (mapView != null) {
                        Log.v("msg", "mapview not empty");
                        locationManager = ((LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE));
                        map = mapView.getMap();
                        if (map == null) {
                            Log.d("", "Map Fragment Not Found or no Map in it!!");
                        }
                        map.clear();
                        try {
                            addMarkers(map);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        map.setIndoorEnabled(true);
                        map.setMyLocationEnabled(true);
                        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                    }
                    break;
                case ConnectionResult.SERVICE_MISSING:
                    // TODO
                    break;
                case ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED:
                    // TODO
                    break;
                default:
                    // TODO
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addMarkers(GoogleMap map) {
        for (Landmark l : landmarks) {
            map.addMarker(l.getMarkerOptions()
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_icon_smaller))
                    .position(l.getPosition())
                    .title(l.getTitle()));
            map.addCircle(l.getCircleOptions());
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        Location location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);

        if (location == null) {
            LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
        } else {
            zoomInOnMyLocation(location);
            handleNewLocation(location);
        }
    }

    protected final synchronized void buildGoogleApiClient() {
        googleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    private void handleNewLocation(Location location) {
        float[] distance = new float[2];
        double currentLatitude = location.getLatitude();
        double currentLongitude = location.getLongitude();
        popup = (LinearLayout) view.findViewById(R.id.popup);
        circleTitle = (TextView) view.findViewById(R.id.title);
        circleInfo = (TextView) view.findViewById(R.id.info);

        for (Landmark l : landmarks) {
            Location.distanceBetween(currentLatitude, currentLongitude, l.getCircleOptions().getCenter().latitude,
                    l.getCircleOptions().getCenter().longitude, distance);
            if (distance[0] < l.getCircleOptions().getRadius()) {
                popup.setVisibility(View.VISIBLE);
                circleTitle.setText(l.getTitle());
                circleInfo.setText(l.getInfo());
                break;
            } else {
                popup.setVisibility(View.INVISIBLE);
            }
        }
    }

    private void zoomInOnMyLocation(Location location) {
        double currentLatitude = location.getLatitude();
        double currentLongitude = location.getLongitude();
        LatLng latLng = new LatLng(currentLatitude, currentLongitude);
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 13));
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i(TAG, "Location Services Suspended to Service, please reconnect");
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                connectionResult.startResolutionForResult(getActivity(),
                        CONNECTION_FAILURE_RESOLUTION_REQUEST);
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
            }
        } else {
            Log.i(TAG, "Location Service Connection Failed with Code " +
                    connectionResult.getErrorCode());
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        googleApiClient.connect();
        locationManager.requestLocationUpdates(provider, 1000, 1, this);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (googleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
            googleApiClient.disconnect();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        googleApiClient.disconnect();
    }

    @Override
    public void onStart() {
        super.onStart();
        googleApiClient.connect();
    }

    @Override
    public void onLocationChanged(Location location) {
        handleNewLocation(location);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onProviderDisabled(String provider) {

        Toast.makeText(getActivity(), "Sätt på gpsen baby", Toast.LENGTH_SHORT).show();
        startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), 0);
    }

}