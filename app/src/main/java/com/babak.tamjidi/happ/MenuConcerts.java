package com.babaktamjidi.happ;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.babaktamjidi.happ.repository.Concert;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class MenuConcerts extends Fragment {

    private static final String TAG = MenuConcerts.class.getSimpleName();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.concerts_layout, container, false);

        ((MainActivity) getActivity()).setActionBarTitle(getString(R.string.title_spelningar));

        ListView listView = (ListView) view.findViewById(R.id.concert_listView);

        ArrayList<Concert> concertList = readFromFile();

        ListAdapter listAdapter = new ConcertAdapter(getActivity(), concertList);

        listView.setAdapter(listAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String[] urls = getResources().getStringArray(R.array.ticket_sale_urls);
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(urls[position]));
                startActivity(browserIntent);
            }
        });
        return view;
    }

    public ArrayList<Concert> readFromFile() {

        ArrayList<Concert> concerts = new ArrayList<>();
        InputStream inputStream = null;
        InputStreamReader inputReader = null;
        BufferedReader bufferedReader;
        String line;
        try {
            inputStream = MainActivity.getGlobalContext().getResources()
                    .openRawResource(R.raw.concert_details);
            inputReader = new InputStreamReader(inputStream);
            bufferedReader = new BufferedReader(inputReader);
            while ((line = bufferedReader.readLine()) != null) {
                String[] concertDetails = line.split("-");
                Concert concert = new Concert();
                concert.setDayName(concertDetails[0]);
                concert.setDayNumber(concertDetails[1]);
                concert.setMonthName(concertDetails[2]);
                concert.setTicketSale(concertDetails[3]);
                concert.setPlaceName(concertDetails[4]);
                concerts.add(concert);
            }
        } catch (IOException e) {
            Log.e(TAG, e.getMessage());
        } finally {
            try {
                if (inputStream != null) {
                    inputStream.close();
                }
                if (inputReader != null) {
                    inputReader.close();
                }
            } catch (IOException e) {
                Log.e(TAG, e.getMessage());
            }
        }
        return concerts;
    }
}