package com.babaktamjidi.happ.repository;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

public class LandmarkMockup {

    private ArrayList<Landmark> landmarks = new ArrayList<>();

    public ArrayList<Landmark> getLandmarks() {
        if (landmarks.isEmpty()) {
            createLandmarks();
        }
        return landmarks;
    }

    private void createLandmarks() {
        Landmark ullevi = new Landmark("Ullevi", new LatLng(57.70591, 11.98723));
        ullevi.setInfo("Den 7 juni 2014 skrevs svensk musikhistoria. " +
                "Då klev Håkan Hellström upp på ett fullsatt Ullevi i Göteborg, med 69 349 personer i publiken " +
                "- fler konsertbesökare än på någon annan konsert i Sverige.");
        Landmark paviljong = new Landmark("L\u00e5ngedrags Paviljong", new LatLng(57.66846, 11.84899));
        paviljong.setInfo("I låten 'Tro och tvivel' från skivan 'För sent för Edelweiss' sjunger Håkan om denna plats. Här kan man äta, ta en fika" +
                " till en utsikt av Göteborgs hamninlopp");
        Landmark hakansuppvaxt = new Landmark("H\u00e5kans uppv\u00e4xt", new LatLng(57.676324, 11.90709));
        hakansuppvaxt.setInfo("I detta området bodde Håkan under sin uppväxt.");
        Landmark dalaskolan = new Landmark("Dalaskolan", new LatLng(57.67877, 11.90114));
        dalaskolan.setInfo("På Dalaskolan spenderade Håkan sin skoltid fram till årkurs 6.");
        Landmark valvet = new Landmark("Valvet", new LatLng(57.6998825, 11.9497953));
        valvet.setInfo("I låten 'Dom där jag kommer ifrån' från skivan '2 steg från paradise' sjunger håkan om valvet scenen. Det var där han för första gången" +
                " såg Freddie - Freddie Wadling som var med på Ullevi när låten spelades.");
        Landmark slottskogsvallen = new Landmark("Slottskogsvallen", new LatLng(57.677758, 11.93958));
        slottskogsvallen.setInfo("Den 8 juni 2013 hade Håkans vårturné nått Göteborg. Över 20 000 fans hade tagit sig till Slottsskogsvallen för att se Håkan" +
                " i sin hemstad.");
        Landmark samskolan = new Landmark("G\u00f6teborgs H\u00f6gre Samskola", new LatLng(57.711926, 11.990583));
        samskolan.setInfo("Håkan var med och bildade bandet Brodel Daniel under skolåren, då han, Henrik Berggren med fler gick på Göteborgs högre samskola.");
        Landmark allen = new Landmark("allén", new LatLng(57.700893, 11.967701));
        allen.setInfo("Nya Allén stod klar 1820. I dag, nästan 200 år senare står den kvar, omsjungen av storheten som Nationalteatern och Håkan.");
        Landmark andralang = new Landmark("Andra L\u00e5nggatan", new LatLng(57.699232, 11.949496));
        andralang.setInfo("En av Göteborgs mest kända gator med ett stort antal barer och en del andra verksamheter bakom fasaderna. Omsjungen av Håkan i t. ex låten" +
                " 'Kom igen Lena'.");
        Landmark mug = new Landmark("MUG", new LatLng(57.7024563, 11.9616408));
        mug.setInfo("Musik utan gränser, en av de mest populära musikaffärerna i Göteborg. Håkan Hellström praktiserade här under sin skolgång och livespelade år 2008" +
                " låten 'För sent för Edelweiss' inne på MUG.");
        Landmark feskekorka = new Landmark("Feskek\u00f6rka", new LatLng(57.7009921, 11.9579404));
        feskekorka.setInfo("Feskekörka, där den står idag invigdes år 1874 och är platsen för dig som gillar fisk och skaldjur. Omtalad i Håkans texter " +
                "och en av de bästa scenerna från filmen 'Känn ingen sorg' utspelar sig i en container bakom körkan.");
        Landmark stibergsliden = new Landmark("Stibergsliden", new LatLng(57.699539, 11.936421));
        stibergsliden.setInfo("Stigbergsliden sträcker sig ca 350 m från Johannesplatsen upp till Stigbergstorget och nämns i Håkans låt 'Zigenarliv Dreamin'' från " +
                "albumet 'För sent för Edelweiss'.");
        Landmark gullbergskaj = new Landmark("Gullbergskaj", new LatLng(57.717973, 11.980164));
        gullbergskaj.setInfo("Kajen belägen i stadsdelen Gullbergsvass, är en plats som Håkan håller kär. Låten 'Mitt Gullbergs kaj paradis' är inspirerad från denna plats." +
                " Kajen benämns också som 'Drömmarnarns kaj");
        Landmark bageriet = new Landmark("P\u00e5\u00e5ls Bageri (P\u00e5gen)", new LatLng(57.6610283, 11.9386589));
        bageriet.setInfo("I låten 'Tro och Tvivel' sjunger håkan om jobbet på bageriet år 1994.");
        Landmark jazzhuset = new Landmark("Jazzhuset", new LatLng(57.6994931, 11.967943));
        jazzhuset.setInfo("'Ja, Jazzhuset är fullt med trix och coca-cola-chicks och kicks!'. Detta nämner Håkan i hitten 'Kom igen Lena' från 2002. " +
                "Jazzhuset är en viktig del i Göteborgs nöjesliv sedan 1977. Tidigare var det enbart en Jazzklubb men nuförtiden så huserar allt från Indiepop, garagerock " +
                "synt och electronica på klubbarna.");

        landmarks.add(paviljong);
        landmarks.add(hakansuppvaxt);
        landmarks.add(dalaskolan);
        landmarks.add(ullevi);
        landmarks.add(valvet);
        landmarks.add(slottskogsvallen);
        landmarks.add(samskolan);
        landmarks.add(allen);
        landmarks.add(andralang);
        landmarks.add(mug);
        landmarks.add(feskekorka);
        landmarks.add(stibergsliden);
        landmarks.add(gullbergskaj);
        landmarks.add(bageriet);
        landmarks.add(jazzhuset);

    }
}
