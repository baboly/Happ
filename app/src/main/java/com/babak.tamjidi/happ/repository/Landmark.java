package com.babaktamjidi.happ.repository;

import android.graphics.Color;

import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class Landmark {

    private String title;
    private LatLng position;
    private Marker marker;
    private MarkerOptions markerOptions;
    private CircleOptions circleOptions;

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    private String info;

    public Landmark(String title, LatLng position) {
        this.title = title;
        this.position = position;
        markerOptions = new MarkerOptions();
        circleOptions = new CircleOptions();
        circleOptions.center(position).radius(150).fillColor(Color.argb(20, 10, 10, 10)).strokeColor(Color.TRANSPARENT);

    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LatLng getPosition() {
        return position;
    }

    public void setPosition(LatLng position) {
        this.position = position;
    }

    public Marker getMarker() {
        return marker;
    }

    public MarkerOptions getMarkerOptions() {
        return markerOptions;
    }

    public CircleOptions getCircleOptions() {
        return circleOptions;
    }

    @Override
    public String toString() {
        return "Landmark{" +
                "title='" + title + '\'' +
                ", position=" + position +
                ", markerOptions=" + markerOptions +
                ", circleOptions=" + circleOptions +
                '}';
    }
}
