package com.babaktamjidi.happ;

public final class Constants {

    private Constants() { /* Instantiation is not allowed */ }

    // Fanmail credentials, used in MenuFanMail
    static final String USERNAME = "fanmail.hakan.hellstrom@gmail.com";
    static final String PASSWORD = "123hakan123";
    static final String FAN_MAIL_INBOX = "fanmail.hakan.hellstrom@gmail.com";

    static final int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    public static final float GEOFENCE_RADIUS_IN_METERS = 100;

    // For this sample, geofences expire after twelve hours
    public static final long GEOFENCE_EXPIRATION_IN_MILLISECONDS = 12 * 60 * 60 * 1000;

}