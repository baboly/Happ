package com.babaktamjidi.happ;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MenuQuiz extends Fragment {

    View mRootView;

    protected Button mAnswerButton_1;
    protected Button mAnswerButton_2;
    protected Button mAnswerButton_3;
    protected Button mAnswerButton_4;

    protected String alternative1;
    protected String alternative2;
    protected String alternative3;
    protected String alternative4;
    protected int rightAnswerIndex;

    protected int mRightAnswerCounter;
    protected int mTotalQuestionsCount;

    protected TextView mQuestionTextView;

    protected QuestionBank mQuestionBank;

    public void askQuestion() {

        mQuestionBank.askAQuestion();
        animateQuestion();
        String[] nextQuestion = mQuestionBank.getNextQuestion();
        String question = nextQuestion[0];
        mTotalQuestionsCount++;

        alternative1 = nextQuestion[1];
        alternative2 = nextQuestion[2];
        alternative3 = nextQuestion[3];
        alternative4 = nextQuestion[4];

        rightAnswerIndex = Integer.parseInt(nextQuestion[5]);

        mQuestionTextView.setText(question);
        mAnswerButton_1.setText(alternative1);
        mAnswerButton_2.setText(alternative2);
        mAnswerButton_3.setText(alternative3);
        mAnswerButton_4.setText(alternative4);
    }

    public void animateQuestion() {
        AlphaAnimation animation = new AlphaAnimation(0, 1);
        animation.setDuration(1500);
        animation.setFillAfter(true);
        mQuestionTextView.setAnimation(animation);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.quiz_layout, container, false);

        ((MainActivity) getActivity()).setActionBarTitle(getString(R.string.title_quiz));

        mRightAnswerCounter = 0;
        mTotalQuestionsCount = 0;
        mQuestionBank = new QuestionBank();

        mQuestionTextView = (TextView) mRootView.findViewById(R.id.question_text_view);
        mAnswerButton_1 = (Button) mRootView.findViewById(R.id.answerButton_1);
        mAnswerButton_2 = (Button) mRootView.findViewById(R.id.answerButton_2);
        mAnswerButton_3 = (Button) mRootView.findViewById(R.id.answerButton_3);
        mAnswerButton_4 = (Button) mRootView.findViewById(R.id.answerButton_4);

        askQuestion();

        mAnswerButton_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rightAnswerIndex == 1) {
                    mRightAnswerCounter++;
                    dialog(true);
                } else {
                    dialog(false);
                }
            }
        });

        mAnswerButton_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rightAnswerIndex == 2) {
                    mRightAnswerCounter++;
                    dialog(true);
                } else {
                    dialog(false);
                }
            }
        });

        mAnswerButton_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rightAnswerIndex == 3) {
                    mRightAnswerCounter++;
                    dialog(true);
                } else {
                    dialog(false);
                }
            }
        });

        mAnswerButton_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rightAnswerIndex == 4) {
                    mRightAnswerCounter++;
                    dialog(true);
                } else {
                    dialog(false);
                }
            }
        });
        return mRootView;
    }

    private void dialog(boolean bool) {
        if(mTotalQuestionsCount == 10) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), AlertDialog.THEME_DEVICE_DEFAULT_DARK);

            builder.setTitle("Game Over").setMessage("Du svarade r\u00e4tt p\u00e5\n"
                    + mRightAnswerCounter + " av 10 fr\u00e5gor.").setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            Intent intent = new Intent(getActivity(), MainActivity.class);
                            startActivity(intent);
                        }
                    });
            AlertDialog dialog = builder.show(); //builder is your just created builder
            TextView messageText = (TextView) dialog.findViewById(android.R.id.message);
            messageText.setGravity(Gravity.CENTER);
            messageText.setTextSize(20);
            dialog.setView(messageText);
            dialog.setCancelable(false);
            messageText.setTextColor(getResources().getColor(R.color.accent));
            dialog.show();
        } else if (bool) {
            Toast.makeText(getActivity(), "R\u00c4TT!\nTotal r\u00e4tt: " + mRightAnswerCounter,
                    Toast.LENGTH_SHORT).show();
            askQuestion();
        } else {
            Toast.makeText(getActivity(), "Fel Svar!", Toast.LENGTH_SHORT).show();
            askQuestion();
        }
    }
}
