package com.babaktamjidi.happ;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;


public class MenuFanMail extends Fragment {

    private LinearLayout fanMailLayout;
    private EditText mailText;
    private EditText senderName;
    private Button mailButton;
    View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fanmail_layout, container, false);

        ((MainActivity) getActivity()).setActionBarTitle(getString(R.string.title_fanmail));

        mailButton = (Button) rootView.findViewById(R.id.send);
        mailButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SendMailTask().execute();
            }
        });

        fanMailLayout = (LinearLayout) rootView.findViewById(R.id.fanmail_layout);
        fanMailLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeKeyboard(getActivity(), container.getWindowToken());
            }
        });
        return rootView;
    }

    public static void closeKeyboard(Context c, IBinder windowToken) {
        InputMethodManager mgr = (InputMethodManager) c.getSystemService(Context.INPUT_METHOD_SERVICE);
        mgr.hideSoftInputFromWindow(windowToken, 0);
    }

    private class SendMailTask extends AsyncTask<Void, Void, Void> {

        private ProgressDialog pd;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = ProgressDialog.show(rootView.getContext(), "Tack för ditt mail", "Skickar", true, false);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            pd.dismiss();
            mailText.setText("");
            senderName.setText("");
        }

        @Override
        protected Void doInBackground(Void... params) {
            Properties props = new Properties();
            props.put("mail.smtp.host", "smtp.gmail.com");
            props.put("mail.smtp.socketFactory.port", "465");
            props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.port", "465");

            Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(Constants.USERNAME, Constants.PASSWORD);
                }
            });

            try {
                mailText = (EditText) rootView.findViewById(R.id.message);
                senderName = (EditText) rootView.findViewById(R.id.sender);
                Message message = new MimeMessage(session);
                message.setFrom(new InternetAddress(Constants.FAN_MAIL_INBOX));
                message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(Constants.FAN_MAIL_INBOX));
                message.setSubject(senderName.getText().toString());
                message.setContent(mailText.getText().toString(), "text/html;charset=utf-8");

                Transport.send(message);

            } catch (MessagingException e) {
                throw new RuntimeException(e);
            }
            return null;
        }
    }

}