package com.babaktamjidi.happ;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Random;
import java.util.Stack;

public class QuestionBank {

    private static final String TAG = QuestionBank.class.getSimpleName();
    private String [] mNextQuestion;
    private Stack <String> mQuestions;

    public QuestionBank() {
        mQuestions = new Stack<>();
        InputStream inputStream = null;
        InputStreamReader inputReader = null;
        BufferedReader bufferedReader;
        String line;
        try {
            inputStream = MainActivity.getGlobalContext().getResources()
                    .openRawResource(R.raw.questions);
            inputReader = new InputStreamReader(inputStream);
            bufferedReader = new BufferedReader(inputReader);
            while (( line = bufferedReader.readLine()) != null) {
                mQuestions.push(line);
            }
        }
        catch (IOException e) {
            Log.i(TAG, e.getMessage());
        }
        finally {
            try {
                if (inputStream != null) {
                    inputStream.close();
                }
                if (inputReader != null) {
                    inputReader.close();
                }
            } catch (IOException e) {
                Log.i(TAG, e.getMessage());
            }
        }
    }

    public void askAQuestion() {
        Random rnd = new Random();
        String line;
        if(!mQuestions.isEmpty()) {
            line = mQuestions.remove(rnd.nextInt(mQuestions.size()));
            mNextQuestion = line.split("-");
        }
    }

    public String[] getNextQuestion() {
        return mNextQuestion;
    }
}
