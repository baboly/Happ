package com.babaktamjidi.happ;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class MenuPhotos extends Fragment {

    View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.photos_layout, container, false);

        ((MainActivity) getActivity()).setActionBarTitle(getString(R.string.title_bilder));
        
        return rootView;
    }
}
