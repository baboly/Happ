package com.babaktamjidi.happ;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.babaktamjidi.happ.repository.Concert;

import java.util.List;

public class ConcertAdapter extends BaseAdapter {

    Context context;
    List<Concert> listConcert;
    LayoutInflater inflater;

    public ConcertAdapter(Context context, List<Concert> listConcert) {
        this.context = context;
        this.listConcert = listConcert;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return listConcert.size();
    }

    @Override
    public Object getItem(int position) {
        return listConcert.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {

            holder = new ViewHolder();
            convertView = this.inflater.inflate(R.layout.concert_row, parent, false);

            holder.concertDayName = (TextView) convertView
                    .findViewById(R.id.concert_day_name);
            holder.concertDayNumber = (TextView) convertView
                    .findViewById(R.id.concert_day_number);
            holder.concertMonthName = (TextView) convertView
                    .findViewById(R.id.concert_month_name);
            holder.concertTicketSale = (TextView) convertView
                    .findViewById(R.id.concert_ticket_sale);
            holder.concertPlaceName = (TextView) convertView
                    .findViewById(R.id.concert_place_name);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Concert concert = listConcert.get(position);

        holder.concertDayName.setText(concert.getDayName());
        holder.concertDayNumber.setText(concert.getDayNumber());
        holder.concertMonthName.setText(concert.getMonthName());
        holder.concertTicketSale.setText(concert.getTicketSale());
        holder.concertPlaceName.setText(concert.getPlaceName());

        return convertView;
    }

    private class ViewHolder {
        TextView concertDayName;
        TextView concertDayNumber;
        TextView concertMonthName;
        TextView concertTicketSale;
        TextView concertPlaceName;
    }
}
